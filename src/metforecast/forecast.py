import pandas as pd
import datetime as dt
import requests
import pytz
from geopy.geocoders import Nominatim
from .exceptions import TooManyInputException, NoLocationException,\
        NoConnectionException, NoForecastNowException
from .font_decoder import text_to_symbol


class ForecastClient:
    """
    Client for fetching data from MET API

    Attributes
    ----------
    user : str
        api.met.no requests a username for the query. This can be an email
        address or repository address.
    loc : str
        Location name or address name. Default is Oslo.
    coords : tuple of float
        Coordinates for the location. Default is None.
        Format : (latitude, longitude)

    """

    def __init__(self, user: str, loc: str = 'Oslo', coords: tuple = None):
        self.user = user

        # If both loc and coords are given
        if loc and coords:
            raise TooManyInputException()

        # If one of them is given
        if coords:
            self.coords = coords
        elif loc:
            self.coords = self._location_to_coords(loc)

        # If neither loc or coords are given
        else:
            raise NoLocationException()

    def _location_to_coords(self, location):
        """find coordinates from location name/address"""
        geopy_client = Nominatim(user_agent=self.user)
        full_loc = geopy_client.geocode(location)
        return full_loc[-1]

    def get_forecast(self):
        """
        Fetch location forecas data from api.met.no

        Returns
        -------
        Response
            If the query was successfull the response object is returned. This
            is a json-like object with data. See more info at
            https://api.met.no/weatherapi/locationforecast/2.0/documentation
            If the query was unsucesfull, None is returned.
        """
        lat, lon = self.coords
        user_agent = {'User-agent': self.user}
        r = requests.get(
                f'https://api.met.no/weatherapi/locationforecast/2.0/compact?'
                f'lat={lat}&lon={lon}',
                headers=user_agent
                )
        if r.status_code == 200:
            return r.json()['properties']['timeseries']
        else:
            raise NoConnectionException(r.reason)

    def get_forecast_dataframe(self, timezone: str = 'Europe/Oslo', icon_font=None):
        """
        timezone : str
            Timezone for which to fetch data. Default: Oslo.
            For UTC, set None.

        Returns
        -------
        df
            A pandas.DataFrame as a timeseries. The columns are all data fethed
            from api.met.no.
        """
        # Fetch data
        data = self.get_forecast()

        # Put data in initial dataframe
        df = pd.DataFrame(data)
        df = df.iloc[:-1, :]
        df['time'] = pd.to_datetime(df['time'])
        if timezone:
            df['time'] = df['time'].dt.tz_convert(timezone)
        df = df.set_index('time')
        inst_keys = list(df['data'].iloc[0]['instant']['details'].keys())
        inst_data = {key: [] for key in inst_keys}
        precipitation = []
        symbol = []
        for i in range(len(df)):
            for key in inst_keys:
                inst_data[key].append(
                        df['data'].iloc[i]['instant']['details'][key]
                        )
            try:
                precipitation.append(
                        df['data'].iloc[i]['next_1_hours']['details']
                        ['precipitation_amount']
                        )
                symbol.append(
                        df['data'].iloc[i]['next_1_hours']['summary']
                        ['symbol_code']
                        )
            except KeyError:
                symbol.append(
                        df['data'].iloc[i]['next_6_hours']['summary']
                        ['symbol_code']
                        )
                precipitation.append(
                        df['data'].iloc[i]['next_6_hours']['details']
                        ['precipitation_amount']/6
                        )
            except KeyError:
                pass
        for key in inst_keys:
            df[key] = inst_data[key]
        df_temp = pd.DataFrame({'precipitation': precipitation,
                                'symbol': symbol
                                },
                               index=df.index
                               )
        df = df.drop(columns='data')
        df_temp = df_temp.resample('H').ffill()
        df = df.resample('H').interpolate()
        df['precipitation'] = df_temp['precipitation']
        df['symbol'] = df_temp['symbol']
        if icon_font:
            decoder = text_to_symbol(icon_font)
            df['symbol'] = df['symbol'].replace(decoder)
        df = df.round(1)
        df.index = df.index.tz_localize(None)
        return df

    def get_forecast_now(self, timezone: str = 'Europe/Oslo', icon_font=None):
        """"
        Get forecast for current hour

        timezone : str
            Timezone for which to fetch data. Default: Oslo
            For UTC, set None.

        Returns
        -------
        pandas.Series
            Weather forecast for current hour.
        """
        if timezone:
            tz = pytz.timezone(timezone)
        else:
            tz = pytz.UTC
        now = dt.datetime.now(tz).replace(minute=0, second=0, microsecond=0)
        now = now.replace(tzinfo=None)
        df = self.get_forecast_dataframe(timezone=timezone, icon_font=icon_font)
        try:
            return df.loc[now]
        except KeyError:
            raise NoForecastNowException(now)
