"""Symbol text to font decode"""
from .exceptions import WrongFontNameException


symbol_decoder = {
        'cloudy': {
            'weather_font': '',
            'nerd_font': '',
            },
        'fog': {
            'weather_font': '',
            'nerd_font': '',
            },
        'clearsky_day': {
            'weather_font': '',
            'nerd_font': '',
            },
        'fair_day': {
            'weather_font': '',
            'nerd_font': '',
            },
        'clearsky_night': {
            'weather_font': '',
            'nerd_font': '',
            },
        'fair_night': {
            'weather_font': '',
            'nerd_font': '',
            },
        'partlycloudy_night': {
            'weather_font': '',
            'nerd_font': '',
            },
        'partlycloudy_day': {
            'weather_font': '',
            'nerd_font': '',
            },
        'rain': {
            'weather_font': '',
            'nerd_font': '',
            },
        'lightrain': {
            'weather_font': '',
            'nerd_font': '',
            },
        'heavyrain': {
            'weather_font': '',
            'nerd_font': '',
            },
        'rainshowers_day': {
            'weather_font': '',
            'nerd_font': '',
            },
        'heavyrainshowers_day': {
            'weather_font': '',
            'nerd_font': '',
            },
        'lightrainshowers_day': {
            'weather_font': '',
            'nerd_font': '',
            },
        'rainshowers_night': {
            'weather_font': '',
            'nerd_font': '',
            },
        'heavyrainshowers_night': {
            'weather_font': '',
            'nerd_font': '',
            },
        'lightrainshowers_night': {
            'weather_font': '',
            'nerd_font': '',
            },
        'sleet': {
            'weather_font': '',
            'nerd_font': '',
            },
        'heavysleet': {
            'weather_font': '',
            'nerd_font': '',
            },
        'lightsleet': {
            'weather_font': '',
            'nerd_font': '',
            },
        'snow': {
            'weather_font': '',
            'nerd_font': '',
            },
        'heavysnow': {
            'weather_font': '',
            'nerd_font': '',
            },
        'lightsnow': {
            'weather_font': '',
            'nerd_font': '',
            },
        'lightrainandthunder': {
            'weather_font': '',
            'nerd_font': '',
            },
        'heavyrainandthunder': {
            'weather_font': '',
            'nerd_font': '',
            },
        'heavyrainshowersandthunder': {
            'weather_font': '',
            'nerd_font': '',
            },
        'rainandthunder': {
            'weather_font': '',
            'nerd_font': '',
            },
        'lightrainandthunder_night': {
            'weather_font': '',
            'nerd_font': '',
            },
        'heavyrainandthunder_night': {
            'weather_font': '',
            'nerd_font': '',
            },
        'heavyrainshowersandthunder_night': {
            'weather_font': '',
            'nerd_font': '',
            },
        'rainandthunder_night': {
            'weather_font': '',
            'nerd_font': '',
            },
}


def text_to_symbol(font_name):
    if font_name not in ['nerd_font', 'weather_font']:
        raise WrongFontNameException
    d = {}
    for x in symbol_decoder:
        d[x] = symbol_decoder[x][font_name]
    return d
