"""Custom Excpetions"""


class NoConnectionException(Exception):
    """Did not connect to MET API"""

    def __init__(self, reason):
        super().__init__(f"Did not connect to MET API. Reason: {reason}")


class NoForecastNowException(Exception):
    """Did not fetch data for current hour"""

    def __init__(self, timestamp):
        super().__init__(f"No forecast for {timestamp}")


class TooManyInputException(Exception):
    """Both location and coords are given"""

    def __init__(self):
        super().__init__("Both loc and coords are given. Only give one.")


class NoLocationException(Exception):
    """Both location and coords are None"""

    def __init__(self):
        super().__init__("Both loc and coords are None. Give one of them.")


class WrongFontNameException(Exception):
    """Wrong icon-font name"""

    def __init__(self):
        super().__init__("Wrong icon-font name given.")
