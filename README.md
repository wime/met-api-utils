# MET Fetcher

This package provides a client to fetch data from from [MET Norways API](https://api.met.no/). 
MET Norway is The Norwegian Meteorological Institute. It is stately funded and 
provides weather data mostly aimed at Norway/The Nordics.

## Purpose
The project was started to create a Python package to collect weather forecast
data. It can be used as a backend for a weather forecast application or to find 
weather forecasts from a shell-script; e.g., to show weather info in a panel. 

The reason for choosing MET's API, is simply that I live in Norway. In addition,
they have an API with a vast amount of data easely available. It is also used 
by something like [GNOME Weather](https://apps.gnome.org/app/org.gnome.Weather/).

## Installation
```
pip install git+https://gitlab.com/wime/met-fetcher.git
```

If you want to clone the repository, the following pip packages are needed:
* pandas
* datetime
* geopy
* requests

## Usage
In order to use MET's API, a user is needed. This can be an email address or repository. The user does not need to be registered at MET, only be applied as an additional argument. Further, the name/adress or coordinates for the wanted weather location are needed.

```python
import metforecast as met

user = "some.user@emmail.com"   # Email address or repository name
location = 'Oslo'               # For Oslo

# Make client
client = met.ForecastClient(user, loc=location) 

# Fetch a pandas dataframe with weather forecast
df = client.get_location_dataframe()
```

## Gratitude
Thanks to the following projects:
* [MET Norway](https://api.met.no/) -   For providing an enormous amount of data through their API.
* [Python](https://www.python.org/) -   For providing a conveniant and easy to use programming language.
* [geopy](https://github.com/geopy/geopy) -   For providing fucntionality to transform location name to coordinates.
